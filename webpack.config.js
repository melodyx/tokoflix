const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './resources/jsx/user.jsx',
	output:{
		path: path.join(__dirname,'/public/'),
		filename:'user2.js'
	},
	devServer: {
	    inline: true,
	    contentBase: __dirname+"/public",
	    compress: false,
	    proxy: {
	      "**": "http://127.0.0.1:8000/",
	    }
	  },
	module:{
		rules:[
			{
				test:/\.jsx$/,
				exclude: /node_modules/,
				use:{
					loader: 'babel-loader'
				}
			}
		]
	}
}