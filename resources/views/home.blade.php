<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Herry Flix</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css" />
        <!-- <link href="/css/app.css" rel="stylesheet" type="text/css" /> -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/custom.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="react-container"></div>
        <script type="text/javascript" src="/user2.js"></script>
    </body>
</html>
