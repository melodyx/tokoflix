import React from 'react';
import ReactDOM from 'react-dom';
import { CookiesProvider } from 'react-cookie';

import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from './components/Home.jsx';
import Detail from './components/Detail.jsx';

import Header from './Header.jsx';
import Footer from './Footer.jsx';

import {Provider} from 'react-redux';
import store from '../redux/stores/store';

ReactDOM.render(
	<CookiesProvider>
		<BrowserRouter>
			<Provider store={store}>
				<div className="page-wrapper">
					<Header/>
					<div className="body">
						<Switch>
							<Route path="/" component={Home} exact/>
							<Route path="/detail/:movieSlug" component={Detail}/>
							<Route  component = {Error}/>
						</Switch>
					</div>
					<Footer />
				</div>
			</Provider>
		</BrowserRouter>
	</CookiesProvider>
	, document.getElementById('react-container'));