import React,{Component } from 'react';
import { Link } from "react-router-dom";
import cookie from 'react-cookies';

import { connect } from 'react-redux';
import { fetchMovies } from '../../redux/actions/movieActions';

class Home extends Component{

	constructor(props){
		super(props);

		this.state={
			lastSearch:null,
		}
	}

	componentWillMount(){
		const page = this.props.location.search?this.props.location.search:"?page=1";
		this.props.fetchMovies(page);
	}

	componentWillReceiveProps(nextProps){
		if(nextProps.location && nextProps.location.search !== this.state.lastSearch){

			let page = (nextProps.location.search && nextProps.location.search !== "")?nextProps.location.search:'?page=1';
			this.props.fetchMovies(page);
			this.setState({
				lastSearch:nextProps.location.search,
			});
			
		}
	}

	createPriceSeparator(priceNumber=0, deleteCents=true, suffix=',-' ,separator='.'){
        if (deleteCents){
            priceNumber = Math.round(priceNumber);
        }
        var priceText = priceNumber.toString();
        priceText = priceText.replace(/\B(?=(\d{3})+(?!\d))/g, separator);
        return priceText + suffix;
    }

    doRupiahPriceSeparator(priceNumber){
        return this.createPriceSeparator(priceNumber,true, ',-', '.');
    }

	render(){

		const movies = this.props.movies.map(movie => {
			
			// stars review helper
				let stars = 0;
				let backgroundPosition = "center top";
				let topPosition = -18;
				for(let x=1;x<=10;x++){
					if(x > movie.vote_average ){ stars=x!=0?x:0; break;}	
					topPosition = -18 * x -2;
					backgroundPosition = "center top "+topPosition+"px"
				}

				let starsStyle = {
					backgroundImage:"url('/css/images/stars-sprite.png')",
					backgroundPosition: backgroundPosition,
					backgroundRepeat:"no-repeat",
					backgroundSize:"100%"
				}

			// stars review helper end


			// price helper
			let price = 3500;
			switch(true){
				case movie.vote_average >= 3 && movie.vote_average <=5:
					price = 8250;
					break;
				case movie.vote_average >= 6 && movie.vote_average <=8:
					price = 16350;
					break;
				case movie.vote_average >= 8 && movie.vote_average <=10:
					price = 21250;
					break;
			}
			// price helper end
			

			// detail slug
				let movieSlug = movie.original_title.replace(" ", "-");	
			// detail slug end

			// my movies check
				let checkBuy = false;
				this.props.myMovies.map(myMovie=>{
					if(myMovie == movie.id){
						checkBuy = true;
					}
				});
			// my movies check end


			return(

				<li key={movie.id} className="col-md-3">
					<div className="box-wrapper" style={{background:'url("https://image.tmdb.org/t/p/w500'+movie.poster_path+'")no-repeat center / cover'}}>
						<Link className="overlay" to={"/detail/"+movie.id+"-"+movieSlug}>
							<h3 className="title">{movie.original_title}</h3>
							<p className="buyed" style={checkBuy?{display:'inline-block'}:null}>Buyed</p>
							<div className="stars" style={starsStyle}></div>
							<p className="price">Rp {this.doRupiahPriceSeparator(price)}</p>
						</Link>
					</div>
				</li>
			)	
		});


		// pagination
			let end =10;
			let start = 1;
			for(let x=1; x<= this.props.totalPages;x++){
				end=x>end?end+10:end;
				start = end - 9;
				if(x == this.props.currentPage){break;}
			}

			let pageNumber = [];


			if(start != 1){
				let prevSection = start - 1;
				pageNumber.push(
					<li key="prevSection">
						<a className="prevSection" href={"?page="+prevSection}>&lt;&lt;</a>
					</li>
				);
			}

			let prev = this.props.currentPage - 1;
			pageNumber.push(
				<li key="prev">
					<Link className="prev" to={"?page="+prev}>&lt;</Link>
				</li>
			);

			while(start<=end){
				pageNumber.push(
					<li key={start}>
						<Link className={start==this.props.currentPage?"active":null} to={"?page="+start}>{start}</Link>
					</li>
				);
				start++;
			}

			let next = this.props.currentPage + 1;
			pageNumber.push(
				<li key="next">
					<Link className="next" to={"?page="+ next}>&gt;</Link>
				</li>
			);

			if(end != this.props.totalPages){
				pageNumber.push(
					<li key="nextSection">
						<Link className="nextSection" to={"?page="+start}>&gt;&gt;</Link>
					</li>
				);
			}
		// pagination end


		return(
			<div className="wrapper home">
				<ul className="row trending-list">
					{movies}
				</ul>
				<div className="pagination">
					<ul className="pagination-number">
						{pageNumber}
						<div className="clear"></div>
					</ul>
				</div>
			</div>	
		);	
	}
}

const mapStateToProps = state =>({
	movies: state.movies.results,
	currentPage:state.movies.currentPage,
	totalPages:state.movies.totalPages,
	myMovies:state.cookie.myMovies
});

export default connect(mapStateToProps,{fetchMovies})(Home);